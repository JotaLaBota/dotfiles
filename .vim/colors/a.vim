" Vim color file
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last Change:	2001 Jul 23

" This is the default color scheme.  It doesn't define the Normal
" highlighting, it uses whatever the colors used to be.

" Set 'background' back to the default.  The value can't always be estimated
" and is then guessed.
hi clear Normal
set bg&

" Remove all existing highlighting and set the defaults.
hi clear

" Load the syntax highlighting defaults, if it's enabled.
if exists("syntax_on")
  syntax reset
endif

let colors_name = "orange"

" vim: sw=2
hi Include ctermbg=NONE ctermfg=6 "Dentro de begin{} end{}
"hi SpecialChar "\\
"hi Special "directorios de imagenes y dentro de $$
"hi LineNR "numeros
"hi Delimiter "{}
hi Statement ctermfg=2 "\begin \end \includegraphics...
hi Number ctermfg=11 "scale=0.23
hi Type ctermfg=2 "textbf, textit...
hi MatchParen ctermbg=NONE ctermfg=5 cterm=underline,bold "highlight de la pareja

hi SpellBad  ctermbg=NONE ctermfg=160 cterm=underline
hi SpellRare  ctermbg=NONE ctermfg=125 cterm=underline
hi SpellCap  ctermbg=NONE ctermfg=125 cterm=underline
hi Delimiter  ctermbg=NONE ctermfg=214 
hi Comment  ctermbg=NONE ctermfg=186 
hi Visual  ctermbg=239 ctermfg=7 
hi Search  ctermbg=239 ctermfg=6 
hi IncSearch  ctermbg=239 ctermfg=6 
hi LineNr  ctermbg=NONE ctermfg=208 




