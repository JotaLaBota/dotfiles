" Vim color file
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last Change:	2001 Jul 23

" This is the default color scheme.  It doesn't define the Normal
" highlighting, it uses whatever the colors used to be.

" Set 'background' back to the default.  The value can't always be estimated
" and is then guessed.
hi clear Normal
set bg&

" Remove all existing highlighting and set the defaults.
hi clear

" Load the syntax highlighting defaults, if it's enabled.
if exists("syntax_on")
  syntax reset
endif

let colors_name = "b"

" vim: sw=2
hi PreCondit ctermbg=NONE ctermfg=6 "Dentro de begin{} end{}
"hi SpecialChar "\\
hi Special ctermfg=3 "directorios de imagenes 
hi LineNR ctermbg=NONE ctermfg=5 "numeros
hi Statement ctermfg=5 "\begin \end \includegraphics...
hi Number ctermfg=11 "scale=0.23
hi Type ctermfg=4 "textbf, textit... fuera del texto y \subset...
hi MatchParen ctermbg=NONE ctermfg=3 cterm=underline,bold "highlight de la pareja
hi Folded ctermbg=1 ctermfg=15
"hi Storage ctermbg=NONE ctermfg=3

hi SpellBad  ctermbg=NONE ctermfg=160 cterm=underline
hi SpellRare  ctermbg=NONE ctermfg=125 cterm=underline
hi SpellCap  ctermbg=NONE ctermfg=125 cterm=underline
hi Delimiter  ctermbg=NONE ctermfg=5
hi Comment  ctermbg=NONE ctermfg=186 
hi Visual  ctermbg=239 ctermfg=2 
hi Search  ctermbg=239 ctermfg=2 
hi IncSearch  ctermbg=3 ctermfg=5
hi Conceal ctermfg=15 ctermbg=NONE



