-------------------------------------------------------------
-- IMPORTS
-------------------------------------------------------------
import XMonad
import XMonad.Layout
import System.Exit
import XMonad.Util.SpawnOnce
import XMonad.Util.Run --spawnPipe
import XMonad.Layout.Spacing
import XMonad.Layout.Maximize
import XMonad.Layout.Spiral
import XMonad.Layout.CenteredMaster
import XMonad.Layout.Dishes
--import XMonad.Layout.MagicFocus


import XMonad.Hooks.ManageDocks -- Evita que la barra quede escondida
import qualified XMonad.StackSet as W
import XMonad.Hooks.ManageHelpers 
import XMonad.Hooks.DynamicLog -- Workspaces in xmobar
import XMonad.Hooks.Place -- This module provides a ManageHook that automatically places floating windows at appropriate positions on the screen.

import qualified Data.Map        as M



-------------------------------------------------------------
-- KEYBINDINGS
-------------------------------------------------------------
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $


    -- Launch programs
    [ ((modm, xK_Return), spawn $ XMonad.terminal conf)
    , ((modm, xK_s), spawn "flameshot && flameshot gui")
    , ((modm, xK_e), spawn "megasync")
    , ((modm, xK_a), spawn "alacritty")
    , ((modm, xK_d), spawn "dmenu_run -nb '#300030' -nf '#ffffff' -sb '#000000' -sf '#fd40ff'")
    , ((modm, xK_p), spawn "passmenu -nb '#300030' -nf '#ffffff' -sb '#000000' -sf '#fd40ff'")
    , ((modm, xK_b), spawn "brave")
    , ((modm, xK_o), spawn "i3lock-fancy")

    -- Move focus
    , ((modm, xK_j), windows W.focusDown)
    , ((modm, xK_k), windows W.focusUp)
    , ((modm, xK_m), windows W.focusMaster)

    -- Swap windows
    , ((modm .|. shiftMask, xK_j), windows W.swapDown)
    , ((modm .|. shiftMask, xK_k), windows W.swapUp)
    , ((modm .|. shiftMask, xK_m), windows W.swapMaster)

    -- Layouts
    , ((modm, xK_space ), sendMessage NextLayout) -- Cycle through the layouts
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf) -- Reset the layouts on the current workspace to default

    -- Resize the master area
    , ((modm, xK_h), sendMessage Shrink)
    , ((modm, xK_l), sendMessage Expand)

    -- Change the number of windows in the master area
    , ((modm, xK_period), sendMessage (IncMasterN 1))
    , ((modm, xK_comma), sendMessage (IncMasterN (-1)))

    -- Other actions
    , ((modm .|. shiftMask, xK_q), kill) -- Close focused window
    , ((modm .|. shiftMask, xK_c), io (exitWith ExitSuccess)) -- Quit xmonad
    , ((modm .|. shiftMask, xK_r), spawn "xmonad --recompile; xmonad --restart") -- Recompile and restart xmonad
    , ((modm, xK_f), withFocused (sendMessage . maximizeRestore)) -- Pseudo-maximize focused window
    , ((modm, xK_t), withFocused $ windows . W.sink) -- Push window back into tiling

    -- Volume
    , ((0, xK_F9),    spawn "pamixer -t")
    , ((0, xK_F10),   spawn "pamixer -d 5")
    , ((0, xK_F11),   spawn "pamixer -i 5")
    ]
    ++

    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move window to workspace N
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

------------------------------------------------------------------------
-- MOUSE BINDINGS
------------------------------------------------------------------------
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-left_click, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-middle_click, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-right_click, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

-------------------------------------------------------------
-- LAYOUTS
-------------------------------------------------------------

-- spacingRaw smartgaps screen_gaps screen_gaps_bool window_gaps window_gaps_bool
myLayoutHook = spacingRaw True (Border 10 10 10 10) False (Border 10 10 10 10) True $
               avoidStruts (maximize (tiled ||| Mirror tiled )) ||| Full
  where
    tiled   = Tall nmaster delta ratio
    -- The default number of windows in the master pane
    nmaster = 1
    -- Default proportion of screen occupied by master pane
    ratio   = 1/2
    -- Percent of screen to increment by when resizing panes
    delta   = 3/100

------------------------------------------------------------------------
-- WINDOW RULES
------------------------------------------------------------------------
-- To find the property name associated with a program, use
-- $ xprop | grep WM_CLASS
-- and click on the client you're interested in.

myManageHook = composeAll
    [ doF W.swapDown                            --New windows inserted below
    , className =? "MEGAsync"       --> placeHook (fixed (1, 1))   
    , className =? "Alacritty"      --> placeHook (fixed (0.5, 0.5))
    , className =? "MEGAsync"       --> doFloat
    , className =? "Alacritty"      --> doFloat
     ]

------------------------------------------------------------------------
-- Event handling
------------------------------------------------------------------------
myEventHook = mempty

------------------------------------------------------------------------
-- STARTUP HOOK
------------------------------------------------------------------------
myStartupHook = do
        spawnOnce "feh --bg-scale ~/Pictures/wallpaper.jpg"
        --spawnOnce "picom"
        spawnOnce "megasync"
        spawnOnce "setxkbmap es"

------------------------------------------------------------------------
-- MAIN FUNCTION
------------------------------------------------------------------------
main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ docks def
       {
      -- Terminal
        terminal           = "st",

      -- Whether focus follows mouse or not
        focusFollowsMouse  = True,

      -- Borders
        borderWidth        = 1,
        normalBorderColor  = "#aaaaaa",
        focusedBorderColor = "#aa00aa",

      -- Modkey
        modMask            = mod1Mask,

      --Workspaces
        workspaces         = ["www","TeX","Doc","Term","Mus","Vid","Unkn","Games","Misc"],

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayoutHook,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,

      -- The next is used to show all my workspaces in xmobar
        logHook = dynamicLogWithPP $ xmobarPP {
            ppOutput = hPutStrLn xmproc
          , ppSep = "   "
          , ppCurrent = xmobarColor "#c3e88d" "" . wrap "[" "]" -- Current workspace in xmobar
          , ppVisible = xmobarColor "#c3e88d" ""                -- Visible but not current workspace
          , ppHidden = xmobarColor "#82AAFF" ""    -- Hidden workspaces in xmobar
          , ppHiddenNoWindows = xmobarColor "#c792ea" ""        -- Hidden workspaces (no windows)
          , ppTitle = mempty -- xmobarColor "#b3afc2" "" . shorten 60     -- Title of active window in xmobar
          , ppLayout = mempty
          , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
      },
        startupHook = myStartupHook
    }
